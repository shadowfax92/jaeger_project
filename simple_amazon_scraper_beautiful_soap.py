from bs4 import BeautifulSoup
import urllib
import sys

# Notes
# Reviews on amazon site are present in in tag <div> under class=reviews under class=drkgry
ufile = urllib.urlopen(sys.stdin.readline().rstrip())
soup = BeautifulSoup(ufile)
#print soup.prettify()
#print soup.find_all("div", {"class":"reviews"}, {"class":"drkgry"})
print soup.find_all("div", {"class":"drkgry"})
